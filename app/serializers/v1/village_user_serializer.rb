class V1::VillageUserSerializer < ActiveModel::Serializer
  attributes :name, :phone, :picture
end
